package ull.modelado.polimorfismo;
//Clase base informático
public class Informatico
{ 	//Atributos
	String m_nombre;
	String m_ocupacion;
	//Constructor/es y métodos
	public Informatico(String a_nombre,String a_ocupacion)
	{	
		m_nombre= a_nombre;
		m_ocupacion = a_ocupacion;
	}
	public Informatico(String a_nombre)
	{
		m_nombre = a_nombre;
		m_ocupacion = "";
	}
	public void informacion()
	{
		System.out.println("Soy "+ m_nombre+ " y soy Informático");
	}
}

