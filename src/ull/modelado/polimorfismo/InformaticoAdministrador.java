package ull.modelado.polimorfismo;
//Clase Inform�tico Administrador de sistema
public class InformaticoAdministrador extends Informatico
{	//constructor/es y m�todos
	public InformaticoAdministrador(String a_nombre)
	{
		super(a_nombre);
		m_ocupacion = "Administrador de sistema";
	}
	@Override
	public void informacion()
	{
		System.out.println("Buenas, soy "+m_nombre+" y mi ocupaci�n es: "+ m_ocupacion);
	}
}
