package ull.modelado.polimorfismo;

public class Main
{
	public static void main(String[] args)
	{
		Informatico t_a = new InformaticoAdministrador("Jose");
		Informatico t_b = new InformaticoProgramador("Marcos");
		
		t_a.informacion();
		t_b.informacion();
	}
}
