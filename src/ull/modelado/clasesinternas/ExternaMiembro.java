package ull.modelado.clasesinternas;

public class ExternaMiembro
{
	String m_str1;
	public ExternaMiembro()
	{
		m_str1 = "Interna de miembro";
		InternaMiembro t_aux = new InternaMiembro();
		t_aux.metodo();
	}
	class InternaMiembro
	{
		void metodo()
		{
			System.out.println(m_str1);
		}
	}
}
