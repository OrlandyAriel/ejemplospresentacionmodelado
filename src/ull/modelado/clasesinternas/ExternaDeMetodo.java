package ull.modelado.clasesinternas;

public class ExternaDeMetodo
{
	String m_str1;
	public ExternaDeMetodo()
	{
		m_str1 = "Interna de m�todo 1";
	}
	public void metodo()
	{
		String t_str2 = "Interna de m�todo 2";
		class InternaDeMetodo
		{
			void metodoDeInterna()
			{
				System.out.println(m_str1);
				System.out.println(t_str2);
			}
		}
		InternaDeMetodo t_aux = new InternaDeMetodo();
		t_aux.metodoDeInterna();
	}
}
