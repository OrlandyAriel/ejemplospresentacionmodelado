package ull.modelado.abstractas;

public class Main
{
	public static void main(String[] args)
	{
	//FORMA INCORRECTA
		//CarreraUniversitaria t_incorrecto1 = new CarreraUniversitaria();
		//Biologia t_incorrecto2 = new CarreraUniversitaria();
	//FORMA CORRECTA
		CarreraUniversitaria t_carrera = new  IngenieriaInformatica();
		t_carrera.estado("Estudio");
		t_carrera.mostar();
		
		t_carrera = new Biologia();
		t_carrera.estado("Tengo");
		t_carrera.mostar();
		
		t_carrera = new IngenieriaMecanica();
		t_carrera.estado("Tengo");
		t_carrera.mostar();
		
		IngenieriaMecanica t_ing = new IngenieriaMecanica();
		t_ing.estado("Estudio");
		t_ing.mostar();;
	}
}

