package ull.modelado.herencia;
//Clase libro que hereda de ReferenciaBibliografia
public class Libro extends ReferenciaBibliografica
{	//Atributos
	private String m_editorial, m_genero, m_ISBN; 
	//Constructor/es y m�todos
	public Libro(String a_titulo, String a_autor, int a_anio,String a_editorial,
	        String a_genero,  String a_ISBN)
	{
		super(a_titulo, a_autor, a_anio);
		m_editorial= a_editorial;
		m_genero = a_genero;
		m_ISBN = a_ISBN;
	}
	@Override
    public String mostrar()
    {
       return "Libro->Titulo:"+getM_titulo()+", Autor:"+getM_autor()+", A�o:"+getM_anio();
    }
	//Getters & Setters
	public String getM_editorial()
	{
		return m_editorial;
	}
	public void setM_editorial(String m_editorial)
	{
		this.m_editorial = m_editorial;
	}
	public String getM_genero()
	{
		return m_genero;
	}
	public void setM_genero(String m_genero)
	{
		this.m_genero = m_genero;
	}
	public String getM_ISBN()
	{
		return m_ISBN;
	}
	public void setM_ISBN(String m_ISBN)
	{
		this.m_ISBN = m_ISBN;
	}
}
