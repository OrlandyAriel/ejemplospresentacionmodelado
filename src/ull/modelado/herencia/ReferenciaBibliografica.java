package ull.modelado.herencia;
//Clase Base para las referencias
public class ReferenciaBibliografica
{ // Atributos
	private String m_titulo, m_autor;
	private int m_anio;
	// constructor/es y m�todos
	public ReferenciaBibliografica(String a_titulo, String a_autor, int a_anio)
	{
		m_titulo = a_titulo;
		m_autor = a_autor;
		m_anio = a_anio;
	}
	public String mostrar()
	{
		return "Referencia->Titulo:"+m_titulo+", Autor:"+m_autor+", A�o:"+m_anio;
	}
	// Getters & Setters
	public String getM_titulo()
	{
		return m_titulo;
	}
	public void setM_titulo(String m_titulo)
	{
		this.m_titulo = m_titulo;
	}
	public String getM_autor()
	{
		return m_autor;
	}
	public void setM_autor(String m_autor)
	{
		this.m_autor = m_autor;
	}
	public int getM_anio()
	{
		return m_anio;
	}
	public void setM_anio(int m_anio)
	{
		this.m_anio = m_anio;
	}
}
