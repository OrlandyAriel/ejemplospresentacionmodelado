package ull.modelado.herencia;
//Clase DocElectronico que hereda de ReferenciaBibliografica
public class DocElectronico extends ReferenciaBibliografica
{	//Atributos
	private String m_lugar_publicacion, m_url;
	//Constructor/es y m�todos
	public DocElectronico(String a_titulo, String a_autor, int a_anio,
	        String a_lugar_publicacion, String a_url)
	{
		super(a_titulo, a_autor, a_anio);
		m_lugar_publicacion = a_lugar_publicacion;
		m_url = a_url;
	}
	@Override
    public String mostrar()
    {
       return "Documento Electronico->Titulo:"+getM_titulo()+", Autor:"+getM_autor()+", A�o:"+getM_anio();
    }
	//Getters & Setters
	public String getM_lugar_publicacion()
	{
		return m_lugar_publicacion;
	}
	public void setM_lugar_publicacion(String m_lugar_publicacion)
	{
		this.m_lugar_publicacion = m_lugar_publicacion;
	}
	public String getM_url()
	{
		return m_url;
	}
	public void setM_url(String m_url)
	{
		this.m_url = m_url;
	}

}
