package ull.modelado.herencia;
public class Main
{
	public static void main(String[] args)
	{
		ReferenciaBibliografica t_ref = new ReferenciaBibliografica("Danza de Dragones", "George R.R Martin",2004);
		ReferenciaBibliografica t_libro = new Libro("La estancia Azul", "Jeffery Deaver", 
	    		2001,"Gigamesh","Narrativa policiaca, Novela negra", "9788466368551");
		Libro t_libro2 =  new Libro("La estancia Azul", "Jeffery Deaver", 
	    		2001,"Gigamesh","Narrativa policiaca, Novela negra", "9788466368551");
		Periodico t_periodico = new Periodico("Un art�culo de los de antes","Risto Mejide",2014,"elPeri�dico",1);
		DocElectronico t_doc_electronico = new DocElectronico("Blaster versus Welchi: Modelado del malware competitivo",
				"Rafael Vida",2015,"Madrid",
				"http://www.elladodelmal.com/2015/11/blaster-versus-welchi-modelado-del.html");
		ReferenciaBibliografica t_ref2 = t_doc_electronico;
		
		System.out.println(t_ref.mostrar());
		System.out.println(t_libro.mostrar());
		System.out.println(t_libro2.mostrar());
		System.out.println(t_periodico.mostrar());
		System.out.println(t_doc_electronico.mostrar());
		System.out.println(t_ref2.mostrar());
		
	}
}
