package ull.modelado.composicion;

public class Main
{
	public static void main(String[] args)
	{
	    Libro t_lib = new Libro("Danza de Dragones", "George R.R Martin", 
	    		2004,"Gigamesh","Narrativa Fantastica", "9788496208582");
	    Lista t_lista= new Lista();
	    t_lista.insertar(t_lib);
	    
		System.out.println(t_lista);
	}
}
