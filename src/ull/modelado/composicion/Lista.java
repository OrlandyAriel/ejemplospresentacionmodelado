package ull.modelado.composicion;
//Clase Lista
public class Lista
{	//Clase interna Nodo
	class Nodo
	{	//Atributos
		private Libro m_valor;
		private Nodo m_siguiente;
		//Constructor/es y m�todos
		public Nodo(Libro a_valor, Nodo a_siguiente)
		{
			m_valor = a_valor;
			m_siguiente = a_siguiente;
		}
		public Libro get_valor()
		{
			return m_valor;
		}
		public Nodo get_siguiente()
		{
			return m_siguiente;
		}
		public void set_valor(Libro a_valor)
		{
			m_valor = a_valor;
		}
		public void set_siguiente(Nodo a_siguiente)
		{
			m_siguiente = a_siguiente;
		}
	}
	//Atributos
	private Nodo m_nodoInicio;
	//Constructor
	public Lista()
	{
		m_nodoInicio = null;
	}
	//M�todos
	public void insertar(Libro a_valor)
	{
		Nodo t_nodo = new Nodo(a_valor, null);
		if (m_nodoInicio != null)
		{
			Nodo t_aux= m_nodoInicio;
			while (t_aux.get_siguiente() != null)
			{
				t_aux = t_aux.get_siguiente();
			}
			t_aux.set_siguiente(t_nodo);
		}
		else {
			m_nodoInicio = t_nodo;
		}
	}
	@Override
	public String toString()
	{
		Nodo t_aux= m_nodoInicio;
		String t_result = t_aux.get_valor().toString();
		while (t_aux.get_siguiente()!= null)
		{	
			t_result += "->" + t_aux.get_valor().toString();
			t_aux = t_aux.get_siguiente();
		}
		return t_result;
	}
}
