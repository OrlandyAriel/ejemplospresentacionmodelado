package ull.modelado.composicion;
//Clase Libro
public class Libro
{	//Atributos
    private String m_titulo, m_autor,m_editorial, m_genero, m_ISBN;
    private int m_anio;
    //Constructor/es y m�todos
    public Libro(String a_titulo, String a_autor, int a_anio,String a_editorial,String a_genero,  String a_ISBN)
    {
        m_titulo = a_titulo;
        m_autor = a_autor;
        m_anio = a_anio;
        m_editorial = a_editorial;
        m_genero = a_genero;
        m_ISBN = a_ISBN;
    }
    @Override
    public String toString()
    {
        return "Libro:"+m_titulo+","+m_autor ;
    }
    //Getters & Setters
	public String getM_titulo()
	{
		return m_titulo;
	}
	public void setM_titulo(String m_titulo)
	{
		this.m_titulo = m_titulo;
	}
	public String getM_autor()
	{
		return m_autor;
	}
	public void setM_autor(String m_autor)
	{
		this.m_autor = m_autor;
	}
	public String getM_editorial()
	{
		return m_editorial;
	}
	public void setM_editorial(String m_editorial)
	{
		this.m_editorial = m_editorial;
	}
	public String getM_genero()
	{
		return m_genero;
	}
	public void setM_genero(String m_genero)
	{
		this.m_genero = m_genero;
	}
	public String getM_ISBN()
	{
		return m_ISBN;
	}
	public void setM_ISBN(String m_ISBN)
	{
		this.m_ISBN = m_ISBN;
	}
	public int getM_anio()
	{
		return m_anio;
	}
	public void setM_anio(int m_anio)
	{
		this.m_anio = m_anio;
	}
}